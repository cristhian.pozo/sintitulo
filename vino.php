<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';
 

$page = filter_input(INPUT_GET, 'page'); 
$pagelimit = 20;

$db = getDbInstance();
if (!$page) {
    $page = 1;
}


$db = getDbInstance(); 
$select = array( 'v_nombre'); 
$db->pageLimit = $pagelimit;
 
$customers = $db->arraybuilder()->paginate("vino", $page, $select);
$total_pages = $db->totalPages;
 
foreach ($customers as $value) {
    foreach ($value as $col_name => $col_value) {
        $filter_options[$col_name] = $col_name;
    } 
    break;
}
include_once 'includes/header.php';
?>
 
<div id="page-wrapper">
    <div class="row">

        <div class="col-lg-6">
            <h1 class="page-header">Menu</h1>
        </div>
        
    </div>
         
    <table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customers as $row) : ?>
                <tr>
	                <td>
                        <a href="vinomenu.php?name=<?php echo htmlspecialchars($row['v_nombre']); ?>">
                            <?php echo htmlspecialchars($row['v_nombre']); ?>
                        </a>
                    </td>
				</tr>	
            <?php endforeach; ?>      
        </tbody>
    </table>
 
    <div class="text-center">

        <?php
        if (!empty($_GET)) { 
            unset($_GET['page']); 
            $http_query = "?" . http_build_query($_GET);
        } else {
            $http_query = "?";
        } 
        if ($total_pages > 1) {
            echo '<ul class="pagination text-center">';
            for ($i = 1; $i <= $total_pages; $i++) {
                ($page == $i) ? $li_class = ' class="active"' : $li_class = "";
                echo '<li' . $li_class . '><a href="customers.php' . $http_query . '&page=' . $i . '">' . $i . '</a></li>';
            }
            echo '</ul></div>';
        }
        ?>
    </div> 

</div>
