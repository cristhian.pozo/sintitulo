<?php
session_start();
require_once './config/config.php';
require_once './includes/auth_validate.php';

if ($_SESSION['admin_type'] !== 'super') { 
    header('HTTP/1.1 401 Unauthorized', true, 401);
    
    exit("401 Unauthorized");
} 
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{ 
    $data_to_store = filter_input_array(INPUT_POST); 
    $db = getDbInstance();
    $last_id = $db->insert ('pizza', $data_to_store);
    
    if($last_id)
    {
    	$_SESSION['success'] = "Customer added successfully!";
    	header('location: comida.php');
    	exit();
    }  
} 
$edit = false;

require_once 'includes/header.php'; 
?>
<div id="page-wrapper">
<div class="row">
     <div class="col-lg-12">
            <h2 class="page-header">Add pizza</h2>
        </div>
        
</div>
    <form class="form" action="" method="post"  id="customer_form" enctype="multipart/form-data">
        <fieldset>
            <div class="form-group">
                <label for="pi_nombres">pizza *</label>
                <input type="text" name="pi_nombre" value="<?php echo $edit ? $customer['pinombre'] : ''; ?>" placeholder="name" class="form-control" required="required" id = "pi_nombre" >
            </div>
            <div class="form-group">
                <label for="pi_id">id *</label>
                <input type="text" name="pi_id" value="<?php echo $edit ? $customer['pi_id'] : ''; ?>" placeholder="id" class="form-control" required="required" id = "pi_id" >
            </div>
            <div class="form-group">
                <label for="tipo">tipo *</label>
                <input type="text" name="tipo" value="<?php echo $edit ? $customer['tipo'] : ''; ?>" placeholder="tipo" class="form-control" required="required" id = "tipo" >
            </div>
            <div class="form-group">
                <label></label>
                <button type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-send"></span></button>
            </div>            
        </fieldset>
    </form>
</div>