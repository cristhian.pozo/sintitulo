<?php
session_start();
require_once './config/config.php'; 
if (isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in'] === TRUE) {
    header('Location:index.php');
} 
if(isset($_COOKIE['name']) && isset($_COOKIE['password']))
{ 
	$username = filter_var($_COOKIE['name']);
	$passwd = filter_var($_COOKIE['password']);
	$db->where ("name", $username);
	$db->where ("passwd", $passwd);
    $row = $db->get('admin_accounts');

    if ($db->count >= 1) 
    { 
        $_SESSION['user_logged_in'] = TRUE;
        $_SESSION['admin_type'] = $row[0]['admin_type'];
        header('Location:index.php');
        exit;
    }
    else 
    {
    unset($_COOKIE['name']);
    unset($_COOKIE['password']);
    setcookie('name', null, -1, '/');
    setcookie('password', null, -1, '/');
    header('Location:login.php');
    exit;
    }
}



include_once 'includes/header.php';
?>
<div id="page-" class="col-md-4 col-md-offset-4">
	<form class="form loginform" method="POST" action="authenticate.php">
		<div class="login-panel panel panel-default">
			<div class="panel-heading">Please Sign in</div>
			<div class="panel-body">
				<div class="form-group">
					<label class="control-label">name</label>
					<input type="text" name="name" class="form-control" required="required">
				</div>
				<div class="form-group">
					<label class="control-label">password</label>
					<input type="password" name="passwd" class="form-control" required="required">
				</div> 
				<button type="submit" class="btn btn-success loginField" >ingresar</button>
				
			</div>
		</div>
	</form>
</div>