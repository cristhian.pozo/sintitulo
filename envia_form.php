<fieldset>
    <legend>contacto</legend>
    <div class="form-group">
        <label class="col-md-4 control-label">User name</label>
        <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input  type="text" name="u_nombre" placeholder="user name" class="form-control" value="<?php echo ($edit) ? $user['u_nombre'] : ''; ?>" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" >Email</label>
        <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" name="email" placeholder="Email" class="form-control" value="<?php echo ($edit) ? $user['email'] : ''; ?>" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" >direccion</label>
        <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" name="direccion" placeholder="direccion" class="form-control" value="<?php echo ($edit) ? $user['direccion'] : ''; ?>" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label" >u_id</label>
        <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" name="u_id" placeholder="u_id" class="form-control" value="<?php echo ($edit) ? $user['u_id'] : ''; ?>" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">
            <button type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-send"></span></button>
        </div>
    </div>
</fieldset>