<?php
session_start();
require_once './config/config.php';
require_once './includes/auth_validate.php';

if ($_SESSION['admin_type'] !== 'super') { 
    header('HTTP/1.1 401 Unauthorized', true, 401);
    
    exit("401 Unauthorized");
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{ 
    $data_to_store = filter_input_array(INPUT_POST); 
    $db = getDbInstance();
    $last_id = $db->insert ('vino', $data_to_store);
    
    if($last_id)
    {
    	$_SESSION['success'] = "Customer added successfully!";
    	header('location: vino.php');
    	exit();
    }  
} 
$edit = false;

require_once 'includes/header.php'; 
?>
<div id="page-wrapper">
<div class="row">
     <div class="col-lg-12">
            <h2 class="page-header">Add vino</h2>
        </div>
        
</div>
    <form class="form" action="" method="post"  id="customer_form" enctype="multipart/form-data">
        <fieldset>
            <div class="form-group">
                <label for="v_nombres">vino *</label>
                <input type="text" name="v_nombre" value="<?php echo $edit ? $customer['v_nombre'] : ''; ?>" placeholder="name" class="form-control" required="required" id = "v_nombre" >
            </div>
            <div class="form-group">
                <label for="v_id">id *</label>
                <input type="text" name="v_id" value="<?php echo $edit ? $customer['v_id'] : ''; ?>" placeholder="id" class="form-control" required="required" id = "v_id" >
            </div>
            <div class="form-group">
                <label for="fecha">fecha *</label>
                <input type="date" name="fecha" value="<?php echo $edit ? $customer['fecha'] : ''; ?>" placeholder="fecha" class="form-control" required="required" id = "fecha" >
            </div>
            <div class="form-group">
                <label></label>
                <button type="submit" class="btn btn-warning" >Save <span class="glyphicon glyphicon-send"></span></button>
            </div>            
        </fieldset>
    </form>
</div>