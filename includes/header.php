<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Tienda</title> 
        <link  rel="stylesheet" href="css/bootstrap.min.css"/> 
        <link href="js/metisMenu/metisMenu.min.css" rel="stylesheet"> 
        <link href="css/sb-admin-2.css" rel="stylesheet"> 
        <link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
        <script src="js/jquery.min.js" type="text/javascript"></script> 

    </head>

    <body>

        <div id="wrapper">
 
            <?php if (isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in'] == true ) : ?>
                <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="dashicons dashicons-phone"></span>
                        </button>
                        <a class="navbar-brand" href="">Mr Wonderful Shop</a>
                    </div> 
                    <ul class="nav navbar-top-links navbar-right"> 
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="contacto.php"><i class="fa fa-gear fa-fw"></i> Contacto</a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                                </li>
                            </ul> 
                        </li> 
                    </ul> 
                    <?php if ( $_SESSION['only_user'] == true ) : ?>
                    
                    <div class="navbar-default sidebar" role="navigation">
                        <div class="sidebar-nav navbar-collapse">
                            <ul class="nav" id="side-menu">
                                <li>
                                    <a href="add_user.php"><i class="fa fa-dashboard fa-fw"></i> REGISTRO</a>
                                </li>

                                <li <?php echo (CURRENT_PAGE =="sodas.php" || CURRENT_PAGE=="comida.php") ? 'class="active"' : '' ; ?>>
                                    <a href="#"><i class="fa fa-user-circle fa-fw"></i> MENU<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                
                                    <li>
                                        <a href="sodas.php"> sodas</a>
                                    </li>
                                    <li>
                                        <a href="agregar_soda.php"><i class="fa fa-plus fa-fw"></i> Agregar sodas</a>
                                    </li>
                                    <li>
                                        <a href="comida.php"> comida</a>
                                    </li>
                                    <li>
                                        <a href="agregar_comida.php"><i class="fa fa-plus fa-fw"></i> Agregar comida</a>
                                    </li>
                                    <li>
                                        <a href="pizza.php"> pizza</a>
                                    </li>
                                    <li>
                                        <a href="agregar_pizza.php"><i class="fa fa-plus fa-fw"></i> Agregar pizza</a>
                                    </li>
                                    <li>
                                        <a href="vino.php">vino</a>
                                    </li>
                                    <li>
                                        <a href="agregar_vino.php"><i class="fa fa-plus fa-fw"></i> Agregar vino</a>
                                    </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="admin_users.php"><i class="fa fa-users"></i> Users</a>
                                </li>
                                <li>
                                    <a href="lista_cliente.php"><i class="fa fa-users"></i>Registered customers</a>
                                </li>
                            </ul>
                        </div>
                        <?php endif; ?> 
                    </div> 
                </nav>
            <?php endif; ?> 
            <?php include_once 'includes/footer.php'; ?>