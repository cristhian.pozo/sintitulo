<?php
session_start();
session_destroy();

if(isset($_COOKIE['name']) && isset($_COOKIE['password'])){
	unset($_COOKIE['name']);
    unset($_COOKIE['password']);
    setcookie('name', null, -1, '/');
    setcookie('password', null, -1, '/');
}
header('Location:index.php');

 ?>